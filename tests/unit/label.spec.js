import { shallowMount } from '@vue/test-utils';
import SFLabel from '@/components/SF-label.vue';

describe('SFLabel.vue', () => {
	it('renders props.params when passed', () => {
		const params = { text: 'label' };
		const wrapper = shallowMount(SFLabel, {
			propsData: { params }
		});
		expect(wrapper.text()).toMatch(params.text);
	});
	it('renders props.msg when passed nothing', () => {
		const params = {};
		const wrapper = shallowMount(SFLabel, {
			propsData: { params }
		});
		expect(wrapper.text()).toMatch('');
	});
	it('renders html well', () => {
		const params = { text: 'name' };
		const wrapper = shallowMount(SFLabel, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe('<label>name</label>');
	});
	it('renders html with attributes well', () => {
		const params = { text: 'name', for: 'username' };
		const wrapper = shallowMount(SFLabel, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe('<label for="username">name</label>');
	});
});
