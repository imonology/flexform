import Vue from 'vue';
import Router from 'vue-router';

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router);

/* Layout */
// import Layout from '../views/layout/Layout';

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/

export const RouterMap = [
	{
		path: '/login',
		component: () => import('@/views/login/index'),
		hidden: true
	},
	{
		path: '/register',
		component: () => import('@/views/register/index'),
		hidden: true
	},
	{ path: '/404', component: () => import('@/views/404'), hidden: true }
];

export const permissionRoute = [
	{
		path: '/_accounts',
		name: 'Account Manager',
		redirect: '/_accounts/list',
		meta: {
			title: 'Account Manager',
			icon: 'user',
			roles: ['admin']
		},
		hidden: false,
		children: [
			{
				path: 'list',
				name: 'List Account',
				type: 'list',
				meta: {
					title: 'Account Manager',
					icon: 'user',
					roles: ['admin']
				}
			},
			{
				path: 'create',
				name: 'Create Account',
				type: 'create',
				hidden: true,
				meta: {
					title: 'Create an account',
					icon: 'user',
					roles: ['admin']
				}
			}
		]
	}
];

const createRouter = () =>
	new Router({
		mode: 'history', // require service support
		scrollBehavior: () => ({ y: 0 }),
		routes: RouterMap
	});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
	const newRouter = createRouter();
	router.matcher = newRouter.matcher; // reset router
}

export default router;
