import { login, logout, getInfo, register } from '@/api/login';
import { getToken, setToken, removeToken, setRoles } from '@/utils/auth';
const getDefaultState = () => {
	return {
		token: getToken(),
		username: '',
		avatar: '',
		roles: []
	};
};
const user = {
	namespaced: true,
	state: getDefaultState(),

	mutations: {
		RESET_STATE: state => {
			Object.assign(state, getDefaultState());
		},
		SET_TOKEN: (state, token) => {
			state.token = token;
		},
		SET_USERNAME: (state, name) => {
			state.username = name;
		},
		SET_AVATAR: (state, avatar) => {
			state.avatar = avatar;
		},
		SET_ROLES: (state, roles) => {
			state.roles = roles;
		}
	},

	actions: {
		// 註冊
		Register({ commit }, userInfo) {
			const username = userInfo.username.trim();
			return new Promise((resolve, reject) => {
				register(username, userInfo.password, userInfo.email)
					.then(response => {
						const data = response.data;
						setToken(data.token);
						commit('SET_TOKEN', data.token);
						resolve();
					})
					.catch(error => {
						reject(error);
					});
			});
		},

		// 登录
		Login({ commit }, userInfo) {
			const username =
				(userInfo.username && userInfo.username.trim()) ||
				(userInfo.account && userInfo.account.trim());
			return new Promise((resolve, reject) => {
				login(username, userInfo.password)
					.then(response => {
						if (response.err === null) {
							const { account, roles } = response.result;
							setToken(account);
							commit('SET_TOKEN', account);
							commit('SET_ROLES', roles);
							setRoles(roles.join());
							commit(
								'SET_AVATAR',
								response.avatar ||
									'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
							);
							resolve(response);
						} else {
							reject(response.err);
						}
					})
					.catch(error => {
						reject(error);
					});
			});
		},

		// 获取用户信息
		GetInfo({ commit, state }) {
			return new Promise((resolve, reject) => {
				getInfo(state.token)
					.then(response => {
						let userInfo = {};
						if (
							response.control.groups &&
							response.control.groups.length > 0
						) {
							// 验证返回的roles是否是一个非空数组
							userInfo.roles = response.control.groups;
							commit('SET_ROLES', response.control.groups);
						} else {
							reject('getInfo: roles must be a non-null array !');
						}
						userInfo.account = response.account;
						commit('SET_USERNAME', response.account);
						commit(
							'SET_AVATAR',
							response.avatar ||
								'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
						);
						resolve(userInfo);
					})
					.catch(error => {
						reject(error);
					});
			});
		},

		// 登出
		LogOut({ commit, state }, token) {
			return new Promise((resolve, reject) => {
				logout(state.token)
					.then(response => {
						if (response.err === null) {
							commit('SET_TOKEN', '');
							commit('SET_ROLES', []);
							removeToken();
							resolve(response);
						} else {
							reject(response.err);
						}
					})
					.catch(error => {
						reject(error);
					});
			});
		},

		// 前端 登出
		FedLogOut({ commit }) {
			return new Promise(resolve => {
				commit('SET_TOKEN', '');
				removeToken();
				resolve();
			});
		},

		// remove token
		resetToken({ commit }) {
			return new Promise(resolve => {
				removeToken(); // must remove  token  first
				commit('RESET_STATE');
				resolve();
			});
		}
	}
};

export default user;
