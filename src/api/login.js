import store from '@/store';
import router from '@/router';
import { getToken } from '@/utils/auth';
import axios from 'axios';

export function register(account, password, email) {
	axios
		.post(
			`${
				process.env.VUE_APP_API_URL ? process.env.VUE_APP_API_URL : ''
			}/api/_account/register`,
			{ account, password, email }
		)
		.then(response => {
			console.log(response);
		})
		.catch(e => console.log(e));
	// return new Promise((resolve, reject) => {
	// 	store.dispatch('sendEvent', {
	// 		name: '_ACCOUNT_REGISTER',
	// 		param: { account, password, email },
	// 		onDone: function(response) {
	// 			if (response.err) {
	// 				reject(response.err);
	// 			} else {
	// 				resolve(response.result);
	// 			}
	// 		},
	// 		mathod: 'POST'
	// 	});
	// });
}

export function login(account, password) {
	axios
		.post(
			`${
				process.env.VUE_APP_API_URL ? process.env.VUE_APP_API_URL : ''
			}/api/_account/login`,
			{ account, password }
		)

		.then(response => {
			if (response.err) {
				Promise.reject(response.err);
			} else {
				Promise.resolve(response);
			}
		})
		.catch(e => console.log(e));
	// return new Promise((resolve, reject) => {
	// 	axios
	// 		.post(`${process.env.VUE_APP_EVENT_API_URL}/_ACCOUNT_LOGIN`, {
	// 			account,
	// 			password
	// 		})
	// 		.then(response => {
	// 			if (response.err) {
	// 				reject(response.err);
	// 			} else {
	// 				resolve(response);
	// 			}
	// 		});
	// });
}

export function getInfo(token = getToken()) {
	if (!token) {
		return Promise.reject('no token');
	}
	axios
		.get(
			`${
				process.env.VUE_APP_API_URL ? process.env.VUE_APP_API_URL : ''
			}/api/_account/${token}`,
			{ account: token, type: 'control' }
		)

		.then(response => {
			if (response.err) {
				Promise.reject(response.err);
			} else {
				Promise.resolve(response);
			}
		})
		.catch(e => console.log(e));
	// return new Promise((resolve, reject) => {
	// 	store.dispatch('sendEvent', {
	// 		name: '_ACCOUNT_GETDATA',
	// 		param: { account: token, type: 'control' },
	// 		onDone: function(response) {
	// 			if (response.err) {
	// 				reject(response.err);
	// 			} else {
	// 				resolve(response.result);
	// 			}
	// 		},
	// 		mathod: 'POST'
	// 	});
	// });
}

export function logout() {
	axios
		.get(
			`${
				process.env.VUE_APP_API_URL ? process.env.VUE_APP_API_URL : ''
			}/api/_account/logout`
		)

		.then(response => {
			if (response.err) {
				Promise.reject(response.err);
			} else {
				Promise.resolve(response);
			}
		})
		.catch(e => console.log(e));
	// return new Promise((resolve, reject) => {
	// 	store.dispatch('sendEvent', {
	// 		name: '_ACCOUNT_LOGOUT',
	// 		param: {},
	// 		onDone: function(response) {
	// 			if (response.err) {
	// 				reject(response.err);
	// 			} else {
	// 				resolve(response);
	// 			}
	// 		},
	// 		mathod: 'POST'
	// 	});
	// });
}
